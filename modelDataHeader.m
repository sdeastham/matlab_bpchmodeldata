classdef modelDataHeader
    %MODELDATAHEADER Object stub used only in loading modelData variables
    
    properties( SetAccess = private )
        simName = '';
        modelName = '';
        fileName = '';
        lowMem = true;
    end
    
    methods (Static)
        function mHead = modelDataHeader(simNameMD,mdlNameMD,fileNameMD,...
                lowMemMD)
            mHead.simName = simNameMD;
            mHead.modelName = mdlNameMD;
            mHead.fileName = fileNameMD;
            mHead.lowMem = lowMemMD;
        end
        
        function mData = loadobj(mHead)
            %mDataHead = modelDataHeader(mData.simName,mData.modelName,...
            %    mData.fileName,mData.lowMem);
            if mHead.lowMem
                memArg = {'lowmem'};
            else
                memArg = {};
            end
            mData = modelData(mHead.simName,mHead.modelName,...
                mHead.fileName,true,memArg{:});
        end
    end
    
end

