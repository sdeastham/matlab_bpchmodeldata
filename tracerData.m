classdef tracerData < handle
    %TRACERDATA Class containing IxJxLxN data.
    %   The tracerData class is designed to hold a timeseries of 3-D data,
    %   with embedded routines for processing and plotting said data.
    
    properties
        % Tracer name
        tracID = '';
        tracName = '';
        % Tracer category
        catID = '';
        catName = '';
        % Sampling times (datenums)
        tVec = []
        % Boundaries of said sampling times
        tEdge = []
        % Unit
        unit = '';
        % Unit as read from file..!
        internalUnit = '';
        % Unit scalar
        unitScale = [];
        % Molar mass (kg/mol)
        molMass = [];
        % Number of samples
        nSamples = [];
        % Grid descriptors
        latEdge = [];
        lonEdge = [];
        altEdge = [];
        pEdge   = [];
        % Is the data defined at cell centers (rather than edges)?
        % This only applies to the vertical dimension
        gridCells = true;
        % File data (generic)
        fileData = struct;
        unitPow = 1;
    end
    
    properties( GetObservable = true )
        % Tracer data
        data = [];
    end
    
    properties (Dependent = true, SetAccess = private)
        % Grid data
        numCell
        pCentre
        altCentre
        latCentre
        lonCentre
        nLat
        nLon
        nLev
        gridDims
        % Name-generating functions and related
        nameGen
        unitGen
        catMini
        tracMini
        ltxName
        ltxUnit
        gridArea
        tSample
        tTick
        tickNames
    end
    
    methods
        % Class constructor
        function tData = tracerData(tracName,catName,fileName)
            if nargin > 0
                if length(tracName)>1 && ~strcmpi(tracName(1:2),'T_')
                    tracName = sprintf('T_%s',tracName);
                end
                if ~strcmpi(catName(1:2),'C_')
                    catName = sprintf('C_%s',catName);
                end
                tData.tracID = tracName;
                tData.catID = catName;
                if (nargin > 2) && (~isempty(fileName))
                    % Read a single tracer into the tracer data class
                    [tData.data,tData.unit,tData.tracName,tData.catName,...
                        tData.tVec, mdlName, mdlRes, tData.unitScale,...
                        tData.molMass,tData.tEdge] = readBPCHSingle(fileName,...
                        tData.catID,tData.tracID);
                    tData.nSamples = length(tData.tVec);
                    [tData.latEdge,tData.lonEdge,tData.altEdge,tData.pEdge] = ...
                        parseGridData(mdlName,mdlRes);
                end
            end
        end
        
        % Display function
        function disp(tData)
            fprintf('Timeseries of 3-D tracer data.\n');
            fprintf('\tName:\t\t\t%s\n',tData.tracName);
            fprintf('\tID:\t\t\t%s\n',tData.tracID);
            fprintf('\tDimensions:\t\t[%ix%ix%i]\n',tData.nLon,...
                tData.nLat,tData.nLev);
            fprintf('\tSamples:\t\t%i\n',tData.nSamples);
            if tData.nSamples > 1
                tSample = tData.tSample;
                if tSample < 1
                    sampleString = sprintf('%5.2f hours',tSample*24);
                else
                    sampleString = sprintf('%0.2f days',tSample);
                end
                fprintf('\tSample period:\t\t%s\n',sampleString);
            end
            fprintf('\tDate range:\t\t%s - %s\n',datestr(tData.tVec(1),'yyyy-mm-dd'),...
                datestr(tData.tVec(end),'yyyy-mm-dd'));
        end
        
        % Dependent property definitions
        function result = get.tSample(tData)
            % Sampling period (days)
            result = mean(diff(tData.tVec));
        end
        
        function result = get.gridArea(tData)
            % Return grid areas in m2
            rEarth = 6.375e6; % in m
            cellSAConst = 2*pi*rEarth*rEarth/tData.nLon;
            result = ones(tData.nLon,tData.nLat);
            for iLat = 1:tData.nLat
                sinDiff = sind(tData.latEdge(iLat+1))-sind(tData.latEdge(iLat));
                cellSA = sinDiff.*cellSAConst; % in m2
                result(:,iLat) = result(:,iLat) .* cellSA;
            end
        end
        
        function result = get.nLon(tData)
            result = length(tData.lonCentre);
        end
        
        function result = get.nLat(tData)
            result = length(tData.latCentre);
        end
        
        function result = get.nLev(tData)
            result = length(tData.pCentre);
        end
        
        function result = get.gridDims(tData)
            result = [tData.nLon tData.nLat tData.nLev tData.nSamples];
        end
        
        function result = get.unitGen(tData)
            if isempty(tData.unit)
                result = @(~) ('-');
            else
                switch lower(tData.unit)
                    case {'cm2/cm3'}
                        result = @(~) ('cm$^2$/cm$^3$');
                    case 'w/m2'
                        result = @(~) ('W/m$^2$');
                    otherwise
                        result = @(tData) (tData.raiseUnit);
                end
            end
        end
        
        function result = get.nameGen(tData)
            switch lower(tData.unit)
                case {'ppbv','ppmv','v/v'}
                    result = @(tData) (makeChem(tData.tracMini));
                otherwise
                    result = @(tData) (tData.tracMini);
            end
        end
        
        function tickPts = get.tTick(tData)
            tickPts = calcTick(tData.tVec(1),tData.tVec(end));
        end
        
        function tickLabels = get.tickNames(tData)
            [~,tickLabels] = calcTick(tData.tVec(1),tData.tVec(end));
        end
        
        function fancyName = get.ltxName(tData)
            fancyName = tData.nameGen(tData);
        end
        
        function fancyUnit = get.ltxUnit(tData)
            fancyUnit = tData.unitGen(tData);
        end
        
        function cMin = get.catMini(tData)
            if ~isempty(tData.catID)
                cMin = tData.catID(3:end);
            else
                cMin = '';
            end
        end
        
        function tMin = get.tracMini(tData)
            if ~isempty(tData.tracID)
                tMin = tData.tracID(3:end);
            else
                tMin = '';
            end
        end
        
        function nCell = get.numCell(tData)
            nCell = tData.nLev*tData.nLon*tData.nLat;
        end
        
        function altCtr = get.altCentre(tData)
            if tData.gridCells
                altCtr = .5.*(tData.altEdge(1:end-1) + tData.altEdge(2:end));
            else
                altCtr = tData.altEdge;
            end
        end
        
        function lonCtr = get.lonCentre(tData)
            lonCtr = .5.*(tData.lonEdge(1:end-1) + tData.lonEdge(2:end));
        end
        
        function latCtr = get.latCentre(tData)
            latCtr = .5.*(tData.latEdge(1:end-1) + tData.latEdge(2:end));
        end
        
        function pCtr = get.pCentre(tData)
            if tData.gridCells
                pCtr = .5.*(tData.pEdge(1:end-1) + tData.pEdge(2:end));
            else
                pCtr = tData.pEdge;
            end
        end
        
        % Operators and native function redefinitions
        function minVal = min(tData,targDim,targDate)
            sglVal = (nargin>1) && ~isempty(targDim);
            if nargin>2
                iSample = tData.parseDate(targDate);
                tempMat = tData.data(:,:,:,iSample);
            else
                tempMat = tData.data;
            end
            if sglVal
                tempMat = tempMat(:);
                targDim = 1;
            end
            minVal = min(tempMat,[],targDim);
        end
        
        function maxVal = max(tData,targDim,targDate)
            sglVal = (nargin>1) && ~isempty(targDim);
            if nargin>2
                iSample = tData.parseDate(targDate);
                tempMat = tData.data(:,:,:,iSample);
            else
                tempMat = tData.data;
            end
            if sglVal
                tempMat = tempMat(:);
                targDim = 1;
            end
            maxVal = max(tempMat,[],targDim);
        end
        
        function meanVal = mean(tData,targDim,targDate)
            sglVal = (nargin>1) && ~isempty(targDim);
            if nargin>2
                iSample = tData.parseDate(targDate);
                tempMat = tData.data(:,:,:,iSample);
            else
                tempMat = tData.data;
            end
            if sglVal
                tempMat = tempMat(:);
                targDim = 1;
            end
            meanVal = mean(tempMat,targDim);
        end
        
        function newTrac = sqrt(aTrac)
            newTrac = aTrac.copyTracer;
            newTrac.unitPow = aTrac.unitPow/2;
            newTrac.data = sqrt(newTrac.data);
        end
        
        function newTrac = plus(aTrac,bTrac)
            if ~strcmpi(class(bTrac),'tracerData')
                % Put non-tracer-class in front
                cTrac = aTrac;
                aTrac = bTrac; 
                bTrac = cTrac;
                clear cTrac;
            end
            switch lower(class(aTrac))
                case 'tracerdata' 
                    newName = sprintf('%s + %s',bTrac.tracMini,aTrac.tracMini);
                    [newTrac,maxSamples] = mergeData(bTrac,aTrac,newName);
                    newTrac.data = bTrac.data(:,:,:,1:maxSamples) + aTrac.data(:,:,:,1:maxSamples);
                case {'double','single','int16','int8','int32','int64'}
                    if numel(aTrac) == 1
                        newName = sprintf('%s + %f',bTrac.tracMini,aTrac);
                    elseif numel(aTrac) == numel(bTrac.data)
                        newName = sprintf('%s + Offset',bTrac.tracMini);
                    else
                        error('plus:mismatchGrid','Grid mismatch when comparing arrays.');
                    end
                    newTrac = bTrac.copyTracer;
                    newTrac.name = newName;
                    newTrac.data = bTrac.data + aTrac;
            end
        end
        
        function newTrac = minus(aTrac,bTrac)
            if ~strcmpi(class(bTrac),'tracerData')
                % Put non-tracer-class in front
                cTrac = aTrac;
                aTrac = bTrac; 
                bTrac = cTrac;
                clear cTrac;
                revMinus = true;
            else
                revMinus = false;
            end
            switch lower(class(aTrac))
                case 'tracerdata'
                    newName = sprintf('%s - %s',aTrac.tracMini,bTrac.tracMini);
                    [newTrac,maxSamples] = mergeData(aTrac,bTrac,newName);
                    newTrac.data = aTrac.data(:,:,:,1:maxSamples) - bTrac.data(:,:,:,1:maxSamples);
                case {'double','single','int16','int8','int32','int64'}
                    tData = bTrac;
                    numData = aTrac;
                    if numel(numData) == 1
                        numString = sprintf('%f',numData);
                    elseif numel(numData) == numel(tData.data)
                        numString = 'Offset';
                    else
                        error('minus:mismatchGrid','Grid mismatch when comparing arrays.');
                    end
                    if revMinus
                        % Tracer - Data
                        numMult = -1;
                        tMult = 1;
                        newName = sprintf('%s - %s',tData.tracMini,numString);
                    else
                        % Data - Tracer
                        numMult = 1;
                        tMult = -1;
                        newName = sprintf('%s - %s',numString,tData.tracMini);
                    end
                    newTrac = tData.copyTracer;
                    newTrac.tracName = newName;
                    newTrac.tracID = 'T_POSTPROC';
                    newTrac.data = (tMult.*tData.data) + (numMult.*numData);
            end
        end
        
        function newTrac = times(aTrac,bTrac)
            if ~strcmpi(class(bTrac),'tracerData')
                % Switch tracers
                cTrac = aTrac;
                aTrac = bTrac; 
                bTrac = cTrac;
                clear cTrac;
            end
            switch lower(class(aTrac))
                case 'tracerdata'
                    newName = sprintf('%s x %s',aTrac.tracMini,bTrac.tracMini);
                    newTrac = mergeData(aTrac,bTrac,newName);
                    newTrac.data = aTrac.data .* bTrac.data;
                    if ~strcmpi('-',newTrac.unit)
                        % Compatible tracers
                        newTrac.unitPow = aTrac.unitPow + bTrac.unitPow;
                    else
                        newTrac.unitPow = 1;
                    end
                case {'double','single','int16','int8','int32','int64'}
                    if numel(aTrac) == 1
                        if isinteger(aTrac)
                            newName = sprintf('%i%s',aTrac,bTrac.tracMini);
                            aTrac = double(aTrac);
                        else
                            newName = sprintf('%5.2f%s',aTrac,bTrac.tracMini);
                        end
                    else
                        newName = bTrac.tracMini;
                    end
                    newTrac = bTrac.copyTracer;
                    newTrac.data = newTrac.data.*aTrac;
                    newTrac.tracName = newName;
                otherwise
                    error('MATLAB:UndefinedFunction','Undefined function ''times'' for input arguments of type ''%s''.',...
                        class(aTrac));
            end
        end
        
        function newTrac = exp(aTrac)
            newTrac = aTrac.copyTracer(true);
            newTrac.data = exp(aTrac.data);
            newTrac.unit = sprintf('exp(%s)',aTrac.unit);
        end
        
        function newTrac = rdivide(aTrac,bTrac)
            switch class(aTrac)
                case 'tracerData'
                    switch class(bTrac)
                        case 'tracerData'
                            newName = sprintf('%s / %s',aTrac.tracMini,bTrac.tracMini);
                            newTrac = mergeData(aTrac,bTrac,newName);
                            newTrac.data = aTrac.data ./ bTrac.data;
                            if ~isempty(newTrac.unit)
                                % Compatible tracers
                                newTrac.unitPow = aTrac.unitPow - bTrac.unitPow;
                            else
                                newTrac.unitPow = 1;
                            end
                        case 'double'
                            % Tracer / number
                            newTrac = aTrac.copyTracer(false);
                            newTrac.tracName = sprintf('%s / %s',aTrac.tracMini,killDec(bTrac,2));
                            newTrac.data = aTrac.data ./ bTrac;
                        otherwise
                            error('MATLAB:UndefinedFunction','Undefined function ''rdivide'' for input arguments of type ''%s''.',...
                                class(aTrac));
                    end
                case 'double'
                    % bData must be the tracer
                    newTrac = bTrac.copyTracer(false);
                    newTrac.unitPow = newTrac.unitPow.*-1;
                    newTrac.tracName = sprintf('%s / %s',killDec(aTrac,2),bTrac.tracMini);
                    newTrac.data = aTrac ./ bTrac.data;
            end
        end
        
        function newTrac = ldivide(aTrac,bTrac)
            newTrac = bTrac./aTrac;
        end
        
        % Class-specific functions
        function data3D = get3DSample(tData,targDate)
            % Extract a single sample
            if nargin > 1
                sampleStr = tData.parseDate(targDate);
                targIdx = {':',':',':',sampleStr};
                data3D = tData.data(targIdx{:});
            else
                % Get mean
                data3D = mean(tData.data,4);
            end
            % No squeeze necessary (singleton dimensions at the end get
            % dropped automatically in MATLAB)
            %data3D = squeeze(data3D);
        end
        
        function outArray = areaSum(tData,varargin)
            % Calculate area-weighted sum of tracer for each layer
            subLon = false;
            subLat = false;
            sampleRange = [1 tData.nSamples];
            levRange = [1 tData.nLev];
            nArg = length(varargin);
            iArg = 1;
            nSkip = 0;
            while iArg <= nArg
                currArg = varargin{iArg};
                switch currArg
                    case 'lonrange'
                        subLon = true;
                        lonRange = varargin{iArg + 1};
                        nPlus = 2;
                    case 'latrange'
                        subLat = true;
                        latRange = varargin{iArg+1};
                        nPlus = 2;
                    case {'surface','sfc'}
                        levRange = 1;
                        nPlus = 1;
                    case 'levrange'
                        levRange = varargin{iArg+1};
                        nPlus = 2;
                    case 'daterange'
                        sampleRange(1) = tData.parseDate(varargin{iArg+1}(1));
                        sampleRange(2) = tData.parseDate(varargin{iArg+1}(end));
                        nPlus = 2;
                    otherwise
                        % Unknown
                        nSkip = nSkip + 1;
                        nPlus = 1;
                end
                iArg = iArg + nPlus;
            end
            if nSkip > 0
                warning('areaSum:badArg','Skipped %i bad arguments',nSkip);
            end
            % Assume grid areas are altitude invariant
            gridArea2D = tData.gridArea;
            if subLat || subLon
                % Restrict the range
                if subLat
                    edgeVec = reshape(tData.latEdge,[1,tData.nLat]);
                    upperEdge = edgeVec(2:end);
                    upperEdge(upperEdge>latRange(end)) = latRange(end);
                    lowerEdge = edgeVec(1:end-1);
                    lowerEdge(lowerEdge<latRange(1)) = latRange(1);
                    gridMult = (upperEdge - lowerEdge)./diff(edgeVec);
                    gridMult(gridMult<0) = 0;
                    gridMult(gridMult>1) = 1;
                    gridMult = repmat(gridMult,[tData.nLon,1]);
                    gridArea2D = gridArea2D.*gridMult;
                end
                if subLon
                    edgeVec = reshape(tData.lonEdge,[tData.nLon,1]);
                    upperEdge = edgeVec(2:end);
                    upperEdge(upperEdge>lonRange(end)) = lonRange(end);
                    lowerEdge = edgeVec(1:end-1);
                    lowerEdge(lowerEdge<lonRange(1)) = lonRange(1);
                    gridMult = (upperEdge - lowerEdge)./diff(edgeVec);
                    gridMult(gridMult<0) = 0;
                    gridMult(gridMult>1) = 1;
                    gridMult = repmat(gridMult,[1,tData.nLat]);
                    gridArea2D = gridArea2D.*gridMult;
                end
            end
            gridAreaSum = sum(gridArea2D(:));
            levRange = levRange(1):levRange(end);
            sampleRange = sampleRange(1):sampleRange(end);
            outArray = zeros(length(levRange),length(sampleRange));
            for iSample = sampleRange
                for iLev = levRange
                    outArray(iLev,iSample) = sum(sum(gridArea2D.*tData.data(:,:,iLev,iSample)))./gridAreaSum;
                end
            end
        end
        
        function data2D = getZonalMean(tData,targDate)
            % Extract zonal mean data
            sampleStr = ':';
            if nargin > 1
                sampleStr = tData.parseDate(targDate);
                getMean = false;
            else
                getMean = true;
            end
            targIdx = {':',':',':',sampleStr};
            data2D = mean(tData.data(targIdx{:}),1);
            if getMean
                data2D = mean(data2D,4);
            end
            data2D = squeeze(data2D);
        end
        
        function data2D = getVertSum(tData,targDate)
            % Calculate vertical sum of data
            sampleStr = ':';
            if nargin > 2
                sampleStr = tData.parseDate(targDate);
                getMean = true;
            else
                getMean = false;
            end
            targIdx = {':',':',':',sampleStr};
            if tData.nSamples == 1
                targIdx = targIdx(1:3);
            end
            data2D = tData.data(targIdx{:});
            if getMean
                data2D = mean(data2D,4);
            end
            data2D = squeeze(sum(data2D,3));
        end
        
        function data2D = getLayerIdx(tData,targLayer,targDate)
            % Extract single layer data
            sampleStr = ':';
            if nargin > 2
                sampleStr = tData.parseDate(targDate);
                getMean = true;
            else
                getMean = false;
            end
            if targLayer == 0
                getSum = true;
                targLayer = ':';
            else
                getSum = false;
            end
            targIdx = {':',':',targLayer,sampleStr};
            if tData.nSamples == 1
                targIdx = targIdx(1:3);
            end
            data2D = tData.data(targIdx{:});
            if getMean
                data2D = mean(data2D,4);
            end
            if getSum
                data2D = sum(data2D,3);
            end
            data2D = squeeze(data2D);
        end
        
        function data2D = getLayerP(tData,targP_hPa,targDate)
            % Extract single layer data (by pressure level)
            if tData.gridCells
                layerIdx = interp1(tData.PCentre,1:tData.nLev,targP_hPa,'nearest');
            else
                % Data is defined at edges
                layerIdx = intepr1(tData.PEdge(1:tData.nLev),1:tData.nLev,targP_hPa,'nearest');
            end
            if nargin > 2
                outArg = {layerIdx,targDate};
            else
                outArg = {layerIdx};
            end
            data2D = tData.getLayerIdx(outArg{:});
        end
        
        function data2D = getLayerAlt(tData,targAlt_km,targDate)
            % Extract single layer data (by pressure altitude)
            if tData.gridCells
                layerIdx = interp1(tData.altCentre,1:tData.nLev,targAlt_km,'nearest');
            else
                layerIdx = interp1(tData.altEdge(1:tData.nLev),1:tData.nLev,targAlt_km,'nearest');
            end
            if nargin > 2
                outArg = {layerIdx,targDate};
            else
                outArg = {layerIdx};
            end
            data2D = tData.getLayerIdx(outArg{:});
        end
        
        function result = layerMean(tData,targLayer,startSample,endSample)
            % Calculate area-weighted layer mean mixing ratios
            if nargin < 4
                endSample = tData.nSamples;
                if nargin < 3
                    startSample = 1;
                end
            end
            miniSamples = endSample + 1 - startSample;
            gridMult = tData.gridArea;
            gridMult = gridMult./sum(gridMult(:));
            if nargin < 2
                targLayer = 1:tData.nLev;
            end
            nLayers = length(targLayer);
            layerOffset = targLayer(1) - 1;
            result = zeros(nLayers,miniSamples);
            fullData = tData.data;
            if parpool('size') > 1
                parfor iLayer = 1:nLayers
                    targLayer = iLayer + layerOffset;
                    resultCore = zeros(1,miniSamples);
                    dataCore = fullData(:,:,targLayer,:);
                    for iSample = startSample:endSample
                        resultCore(1,iSample+1-startSample) = sum(sum(...
                            gridMult.*dataCore(:,:,1,iSample)));
                    end
                    result(iLayer,:) = resultCore;
                end
            else
                for iLayer = 1:nLayers
                    targLayer = iLayer + layerOffset;
                    resultCore = zeros(1,miniSamples);
                    dataCore = fullData(:,:,targLayer,:);
                    for iSample = startSample:endSample
                        resultCore(1,iSample+1-startSample) = sum(sum(...
                            gridMult.*dataCore(:,:,1,iSample)));
                    end
                    result(iLayer,:) = resultCore;
                end
            end
        end
        
        function outData = copyTracer(inData,noData)
            outData = tracerData();
            varList = {'tracID','tracName','catID','catName',...
                'tVec','unit','unitScale','molMass','nSamples',...
                'latEdge','lonEdge','altEdge','pEdge','fileData',...
                'unitPow','tEdge','gridCells'};
            nVar = length(varList);
            for iVar = 1:nVar
                outData.(varList{iVar}) = inData.(varList{iVar});
            end
            if nargin < 2 || ~noData
                outData.data = inData.data;
            else
                outData.data = [];
            end
        end
        
        function fancyUnit = raiseUnit(tData)
            switch tData.unitPow
                case 0
                    fancyUnit = '-';
                case 1
                    fancyUnit = tData.unit;
                otherwise
                    fancyUnit = sprintf('(%s)$^{%i}$',tData.unit,tData.unitPow);
            end
        end
        
        function padTrac(smlTrac,bigTrac)
            % Pad out tracer data with zeros to match larger data set
            smlDims = smlTrac.gridDims;
            bigDims = bigTrac.gridDims;
            misMatch = find(bigDims ~= smlDims);
            minMax = [1,1,1;bigDims(1:3)];
            for iDim = 1:length(misMatch)
                currDim = misMatch(iDim);
                switch currDim
                    case 3
                        bigVec = bigTrac.altCentre;
                        smlVec = smlTrac.altCentre;
                    case 2
                        bigVec = bigTrac.latCentre;
                        smlVec = smlTrac.latCentre;
                    case 1
                        bigVec = bigTrac.lonCentre;
                        smlVec = smlTrac.lonCentre;
                end
                minMax(1,currDim) = find(bigVec==min(smlVec),1,'first');
                minMax(2,currDim) = find(bigVec==max(smlVec),1,'first');
            end
            newData = zeros(size(bigTrac.data));
            newData(minMax(1,1):minMax(2,1),minMax(1,2):minMax(2,2),...
                minMax(1,3):minMax(2,3),:) = smlTrac.data;
            smlTrac.data = newData;
            clear newData;
            smlTrac.lonEdge = bigTrac.lonEdge;
            smlTrac.latEdge = bigTrac.latEdge;
            smlTrac.altEdge = bigTrac.altEdge;
            smlTrac.pEdge = bigTrac.pEdge;
        end
        
        function [newTrac,maxSamples] = mergeData(aTrac,bTrac,newName)
            % Combine basic properties for element-wise operations
            newTrac = tracerData();
            if aTrac.nSamples ~= bTrac.nSamples
                warning('operator:sampleMismatch','Tracer sample counts differ. Using minimum common count.');
                maxSamples = min([aTrac.nSamples,bTrac.nSamples]);
            else
                maxSamples = aTrac.nSamples;
            end
            if aTrac.numCell ~= bTrac.numCell
                error('operator:dimensionMismatch','Tracer grids differ.');
            end
            
            newTrac.altEdge  = aTrac.altEdge;
            newTrac.pEdge    = aTrac.pEdge;
            newTrac.latEdge  = aTrac.latEdge;
            newTrac.lonEdge  = aTrac.lonEdge;
            newTrac.nSamples = maxSamples;
            newTrac.tracName = newName;
            newTrac.tracID = 'T_POSTPROC';
            if strcmpi(aTrac.unit,bTrac.unit)
                newTrac.unit = aTrac.unit;
                newTrac.unitScale = aTrac.unitScale;
                newTrac.unitPow = aTrac.unitPow;
            else
                newTrac.unit = '-';
                newTrac.unitScale = 1;
                newTrac.unitPow = 1;
            end
            if (aTrac.molMass==bTrac.molMass)
                newTrac.molMass = aTrac.molMass;
            else
                newTrac.molMass = 1;
            end
            if all(aTrac.tVec(1:maxSamples)==bTrac.tVec(1:maxSamples))
                newTrac.tVec = aTrac.tVec(1:maxSamples);
                newTrac.tEdge = aTrac.tEdge(1:(maxSamples+1));
            else
                % Not sure this even makes sense...
                newTrac.tEdge = linspace(0,1,newTrac.nSamples+1);
                newTrac.tVec = .5.*(newTrac.tEdge(1:end-1)+newTrac.tEdge(2:end));
            end
            if strcmpi(aTrac.catName,bTrac.catName)
                newTrac.catName = aTrac.catName;
                newTrac.catID = aTrac.catID;
            else
                newTrac.catName = 'Post-processed tracer';
                newTrac.catID = 'C_POSTPROC';
            end
        end
        
        % Plotting functions
        function vidZonal(tData,varargin)
            nArg = length(varargin);
            iArg = 1;
            startSample = 1;
            endSample = tData.nSamples;
            tVecPlot = tData.tVec;
            nSkip = 0;
            genOut = false;
            while iArg <= nArg
                currArg = varargin{iArg};
                switch lower(currArg)
                    case 'startdate'
                        startSample = tData.parseDate(varargin{iArg+1});
                        nPlus = 2;
                    case 'enddate'
                        endSample = tData.parseDate(varargin{iArg+1});
                        nPlus = 2;
                    case 'filename'
                        fileName = varargin{iArg+1};
                        genOut = true;
                        nPlus = 2;
                    otherwise
                        nSkip = nSkip + 1;
                        nPlus = 1;
                end
                iArg = iArg + nPlus;
            end
            if nSkip > 0
                warning('vidZonal:badArg','Skipped %i bad arguments.',nSkip);
            end
            tVecPlot = tVecPlot(startSample:endSample);
            % Plot initial
            [hFig,hAx,hPlot] = tData.plotZonal(tVecPlot(1));
            
            % Figure out date title format
            meanInterval = mean(diff(tVecPlot));
            if meanInterval < 1
                % Show hours
                dateFormat = 'yyyy-mm-dd HH:MM';
            elseif meanInterval > 20
                % Show months
                dateFormat = 'yyyy mm dd';
            else
                % Show days
                dateFormat = 'yyyy mmm dd';
            end
            
            % Is the data defined for cells or for edges?
            
            % Generate video object
            if genOut
                vidObj = VideoWriter([fileName '.avi']);
                vidObj.Quality = 90;
                vidObj.FrameRate = 12;
                open(vidObj);
            end
            
            % Set color range
            cMin = +Inf;
            cMax = -Inf;
            for iSample = startSample:endSample
                currData = mean(tData.data(:,:,:,iSample),1);
                currData = currData(:);
                cMin = min([cMin,min(currData)]);
                cMax = max([cMax,max(currData)]);
            end
            
            if cMin > 0
                cMin = 0;
            elseif cMax < 0
                cMax = 0;
            end
            cLims = [cMin cMax];
            
            for iSample = 1:length(tVecPlot)
                getSample = iSample + startSample - 1;
                currData = squeeze(mean(tData.data(:,:,:,getSample),1));
                setMapData(hPlot,currData');
                set(hAx,'clim',cLims);
                title(datestr(tVecPlot(iSample),dateFormat));
                drawnow;
                if genOut
                    currFrame = im2frame(hardcopy(hFig, '-dzbuffer', '-r0'));
                    writeVideo(vidObj,currFrame);
                end
            end
            
            if genOut
                close(vidObj);
                close(hFig);
            end
        end
        
        function [hFig,hAxVec,hPlotVec] = plotZonal(tData,plotDate,maxLev)
            if nargin > 1
                if ischar(plotDate)
                    iSample = tData.parseDate(plotDate);
                    plotData = tData.data(:,:,:,iSample);
                    plotString = sprintf('%s %s',tData.tracMini,...
                        datestr(tData.tVec(iSample),'yymmdd-HHMM'));
                else
                    % Multiple dates given, either as vector of datenums or
                    % as cell array of strings; take mean
                    nDates = length(plotDate);
                    sampleVec = zeros(nDates,1);
                    if iscell(plotDate)
                        for iDate = 1:nDates
                            sampleVec(iDate) = tData.parseDate(plotDate{iDate});
                        end
                    else
                        for iDate = 1:nDates
                            sampleVec(iDate) = tData.parseDate(plotDate(iDate));
                        end
                    end
                    plotData = mean(tData.data(:,:,:,min(sampleVec):max(sampleVec)),4);
                    plotString = sprintf('%s %s',tData.tracMini,...
                        datestr(mean(tData.tVec(sampleVec)),'yymmdd-HHMM'));
                end
            elseif tData.nSamples == 1
                % Unambiguous, really
                plotData = tData.data(:,:,:,1);
                plotString = sprintf('%s %s',tData.tracMini,...
                    datestr(tData.tVec,'yymmdd-HHMM'));
            else
                error('tracerData:badArg','No target date given.');
            end

            if nargin < 3
                maxLev = tData.nLev;
            end
            
            [hFig,hAxVec,hPlotVec] = plotGrid(plotData(:,:,1:maxLev),'zonal',...
                'latedge',tData.latEdge,'gridcells',tData.gridCells,'altedge',...
                tData.altEdge(1:maxLev+tData.gridCells),'unit',tData.ltxUnit);
            set(hFig,'name',plotString);
            %{
            plotGridData(plotData,1,0,...
                false,tData.ltxUnit,'latvals',tData.latEdge,'lonvals',...
                tData.lonEdge,'altvals',tData.altEdge);
            %}
        end
        
        function plotLayer(tData,targLayer,plotDate)
            if nargin < 3
                % Use mean
                sampleIdx = {':',':',':',':'};
            else
                iSample = tData.parseDate(plotDate);
                sampleIdx = {':',':',':',iSample};
            end
            
            dataIn = tData.data(sampleIdx{:});
            
            % Allow for column totals
            if targLayer == 0
                dataIn = transpose(sum(dataIn,3));
                targLayer = 1;
                titleString = sprintf('%sTotal',tData.tracMini);
            else
                titleString = sprintf('%s_%4.2fkm',tData.tracMini,tData.altCentre(targLayer));
                if tData.nLev == 1
                    % Need to transpose
                    dataIn = dataIn';
                end
            end

            plotGrid(dataIn,'layer',...
                'ilayer',targLayer,'plotUnit',tData.ltxUnit,...
                'latvals',tData.latEdge,'lonvals',tData.lonEdge);
            set(gcf,'name',titleString);
            %plotGridData(tData.data(sampleIdx{:}),0,targLayer,...
            %    false,tData.ltxUnit,'latvals',tData.latEdge,'lonvals',tData.lonEdge,...
            %    'altvals',tData.altEdge);
        end
        
        function [contourData] = plotMeanSeries(tData,varargin)
            % Parse inputs
            iArg = 1;
            nArg = length(varargin);
            startSample = 1;
            endSample = tData.nSamples;
            nSkip = 0;
            levRange = [1 tData.nLev];
            newRange = levRange;
            while iArg <= nArg
                currArg = varargin{iArg};
                switch lower(currArg)
                    case 'startdate'
                        startSample = tData.parseDate(varargin{iArg+1});
                        nPlus = 2;
                    case 'enddate'
                        endSample = tData.parseDate(varargin{iArg+1});
                        nPlus = 2;
                    case 'levrange'
                        newRange = varargin{iArg+1};
                        if numel(newRange) == 1
                            newRange = ones(1,2).*newRange;
                        end
                        nPlus = 2;
                    case 'minlev'
                        newRange(1) = varargin{iArg+1};
                        nPlus = 2;
                    case 'maxlev'
                        newRange(2) = varargin{iArg+1};
                        nPlus = 2;
                    otherwise
                        nSkip = nSkip + 1;
                        nPlus = 1;
                end
                iArg = iArg + nPlus;
            end
            
            if newRange(2) > levRange(2)
                warning('plotMeanSeries:badRange','Input data has only %i levels',levRange(2));
                newRange(2) = levRange(2);
            end
            if newRange(1) < levRange(1)
                warning('plotMeanSeries:badRange','Data starts at level %i',levRange(1));
                newRange(1) = levRange(1);
            end
            if newRange(2) < newRange(1)
                % This is ambiguous, so terminate
                error('plotMeanSeries:badRange','Level range must be monotonically increasing');
            end
            levRange = newRange;
            
            % Any issues?
            if nSkip > 0
                warning('plotMeanSeries:badArg','Skipped %i bad arguments',nSkip);
            end
            
            nLevPlot = diff(levRange) + 1;
            
            % Is this a contour plot or a single layer series?
            isContour = nLevPlot > 1;
            
            % Calculate layer means for each sample
            miniSamples = endSample + 1 - startSample;
            contourData = zeros(nLevPlot + 1,miniSamples + 1);
            contourData(1:end-1,1:end-1) = tData.layerMean(levRange(1):levRange(end),startSample,endSample);
            
            % Plot data
            figure('windowstyle','docked','color',[1 1 1]);
            plotAx = axes('position',[.1 .1 .8 .8]);
            
            if isContour
                mapHandle = pcolor(plotAx,tData.tEdge(startSample:endSample+1),...
                    tData.altEdge,contourData);
                set(mapHandle,'edgecolor','none');
                set(plotAx,'ylim',[min(tData.altEdge),max(tData.altEdge)]);
                ylabel(plotAx,'Altitude (km)','interpreter','latex','fontsize',16);
                cBarHandle = colorbar;
                xlabel(cBarHandle,tData.ltxUnit,'interpreter','latex','fontsize',12);
                set(cBarHandle,'fontsize',12);
            else
                % Data is 1D, so plot as a line
                contourData = contourData(1,1:end-1);
                line('XData',tData.tVec(startSample:endSample),...
                    'YData',contourData,'linewidth',2);
                ylabel(plotAx,sprintf('Mean %s (%4.2f-%4.2f km) (ppbv)',...
                    tData.ltxName,tData.altEdge(levRange(1)),...
                    tData.altEdge(levRange(2))),'interpreter','latex','fontsize',16);
            end
            set(plotAx,'xtick',tData.tTick,'xticklabel',...
                tData.tickNames,'box','on',...
                'XGrid','on','YGrid','on','layer','top','color','none',...
                'fontsize',12,'xlim',...
                [tData.tVec(startSample),tData.tVec(endSample)]);
        end
        
        function [iSample] = parseDate(tData,plotDate)
            if ischar(plotDate)
                % Assume yyyymmddHHMMSS format
                % Go to midday for daily data, midmonth for monthly
                addQuant = 0;
                switch length(plotDate)
                    case 6
                        % Monthly
                        plotDate = sprintf('%s01',plotDate);
                        formStr = 'yyyymmdd';
                        tLow = datevec(plotDate,formStr);
                        addQuant = eomday(tLow(1),tLow(2))/2;
                    case 8
                        formStr = 'yyyymmdd';
                        addQuant = 0.5;
                    case 10
                        formStr = 'yyyymmddHH';
                        addQuant = 0.5/24;
                    case 12
                        formStr = 'yyyymmddHHMM';
                    case 14
                        formStr = 'yyyymmddHHMMSS';
                    otherwise
                        error('tracerData:parseDate','Unknown date format.');
                end
                plotDate = addQuant + datenum(plotDate,formStr);
            end
            % Round to seconds
            errTol = 1/(24*3600);
            if (plotDate - max(tData.tVec)) > errTol
                iSample = tData.nSamples;
                warning('parseDate:lastSample','Using final sample; plot date outside sampling range (%s-%s).',...
                    datestr(tData.tVec(1)),datestr(tData.tVec(end)));
            elseif (min(tData.tVec) - plotDate) > errTol
                iSample = 1;
                warning('parseDate:firstSample','Using first sample; plot date outside sampling range (%s-%s).',...
                    datestr(tData.tVec(1)),datestr(tData.tVec(end)));
            elseif tData.nSamples == 1
                % Only one sample
                iSample = 1;
            else
                %iSample = find(tData.tVec >= plotDate,1,'first');
                % Use nearest-neighbour
                iSample = interp1(tData.tVec,1:tData.nSamples,plotDate,'nearest');
            end
        end
        
    end
    
end

