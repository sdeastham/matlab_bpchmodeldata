function [ nBytes ] = writeFORTRANRecord( fileID,data,dataStr,noWrite)
%WRITEFORTRANRECORD Write data to a FORTRAN binary data file (BPCH)
%   fileID:     MATLAB file ID
%   data:       Data to be written
%   dataStr:    The type of the data to be written

% Check that the file ID is OK
if fileID < 0
    error('writeFORTRANRecord:InvalidFile','Invalid file identifier');
end

if iscell(data)
    % Write a mixed-data record
    % First calculate record length
    nBytes = 0;
    for iEntry = 1:numel(data)
        miniData = data{iEntry};
        miniStr = dataStr{iEntry};
        nBytes = nBytes + writeFORTRANRecord(fileID,miniData,miniStr,true);
    end
    % Now write the records
    fwrite(fileID,nBytes,'int32',0,'ieee-be');
    skipNum = 0;
    for iEntry = 1:numel(data)
        currBlock = data{iEntry};
        currStr = dataStr{iEntry};
        fwrite(fileID,currBlock(:),currStr,skipNum,'ieee-be');
        %{
        for iData = 1:numel(currBlock)
            currData = currBlock(iData);
            fwrite(fileID,currData,currStr,skipNum,'ieee-be');
        end
        %}
    end
    fwrite(fileID,nBytes,'int32',0,'ieee-be');
    return;
end

if nargin<3 || isempty(dataStr)
    dataStr = 'int32';
    dTypeSize=4;
else
    dataStr = lower(strtrim(dataStr));
    badType = false;
    if strcmpi(dataStr,'char')
        % Using a 8-bit standard.. for now
        warning('writeFORTRANRecord:assume8','Assuming 8-bit char');
        dataStr = 'char*1';
        dTypeSize = 1;
    elseif length(dataStr)>2 && strcmp(dataStr(end-1),'*')
        dTypeSize = str2double(dataStr(end));
    elseif length(dataStr)>3 && strcmp(dataStr(1:3),'bit')
        dTypeSize = str2double(dataStr(4:end));
    elseif length(dataStr)>4 && strcmp(dataStr(1:4),'ubit')
        dTypeSize = str2double(dataStr(5:end));
    else
        switch lower(dataStr)
            case {'uint8','uchar','unsigned char','int8','integer*1','schar','signed char','char*1'}
                dTypeSize = 1;
            case {'uint16','ushort','int16','integer*2','short'}
                dTypeSize = 2;
            case {'uint','uint32','ulong','int32','integer*4','long','single','float','float32','real*4'}
                dTypeSize = 4;
            case {'uint64','int64','integer*8','double','float64','real*8'}
                dTypeSize = 8;
            otherwise
                badType = true;
        end
    end
    if badType
        error('writeFORTRANRecord:badSize','Datatype ''%s'' not recognized',dataStr);
    end
end

if nargin < 4
    noWrite = false;
end

% How long is this data?
%{
testData = cast(data(1),dataStr); %#ok<NASGU>
sData = whos('testData');
dTypeSize = sData.bytes;
%}

%data=(fread(fileID,(rLen-adjustLen)/dTypeSize,dataStr));
%precisionStr = sprintf('%s%i%',dataStr,dTypeSize);
% First write a single int32 header saying how long the data is
% Need to write at the end as well..
nBytes = numel(data)*dTypeSize;
if ~noWrite
    fwrite(fileID,nBytes,'int32',0,'ieee-be');
    % Reshape the data?
    
    % DEBUG
    %{
    tellPt = ftell(fileID);
    %}
    
    fwrite(fileID,data(:),dataStr,0,'ieee-be');
    
    %{
    fseek(fileID,tellPt,-1);
    outTest = fread(fileID,nBytes/dTypeSize,dataStr,0,'ieee-be');
    if strcmpi(dataStr,'uchar')
        outTest = cast(outTest,'char');
    end
    %}
    
    %disp(outTest);
    %{
    for iData = 1:numel(data)
        skipNum = 0;
        currData = data(iData);
        fwrite(fileID,data(:),dataStr,skipNum,'ieee-be');
    end
    %}
    fwrite(fileID,nBytes,'int32',0,'ieee-be');
end
end

