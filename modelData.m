classdef modelData < handle
    %MODELDATA Class containing CTM data taken from a ctm.bpch file.
    %   Detailed explanation goes here
    
    properties
        % Header data
        simName = ''
        modelName = ''
        fileName = '';
        tracerFileName = '';
        diagFileName = '';
        fileDate = 0;
        tracerFileDate = 0;
        diagFileDate = 0;
        isMeta = false;
        halfPolar = true;
        center180 = true;
        
        % Data storage type
        dType = 'single';
        
        startDate = '';
        endDate = ''
        
        % Co-ordinate system data
        metModel = '';
        latEdge = [];
        lonEdge = [];
        pEdge = [];
        altEdge = [];
        latRes = '';
        lonRes = '';
        
        nLat = [];
        nLon = [];
        nLev = [];
        
        % Grid specification object
        gSpec = [];
        
        % Timing data
        tEdge = [];
        tVec = [];
        %tTick = [];
        %tickNames = [];
        
        % Tracer data
        tracData
        nMeta = 0; % Number of tracers for which we have metadata
        nCat; % Number of categories
    end
    properties( SetAccess = private )
        % Metadata
        dataProc = false;
        % If the lowMem property is set to true, only one set of tracerData
        % is stored at a time
        lowMem = false;
        lastCat = '';
        lastTrac = '';
    end
    
    methods
        
        %{
        function mdlOut = saveobj(mdlIn)
            % Don't save actual tracer data
            mdlOut = modelData(mdlIn.simName,mdlIn.modelName,...
                mdlIn.fileName,mdlIn.nMeta > 0);
        end
        %}
        function clearTracers(mData)
            catNames = fieldnames(mData.tracData);
            for iCat = 1:mData.nCat
                currCat = catNames{iCat};
                tracNames = fieldnames(mData.tracData.(currCat));
                nTrac = length(tracNames);
                for iTrac = 1:nTrac
                    currTrac = tracNames{iTrac};
                    mData.tracData.(currCat).(currTrac).data = [];
                end
            end
        end
        
        function lowMemMode(mData,newMode)
            if newMode ~= mData.lowMem
                if newMode
                    % Clear all existing tracers
                    mData.clearTracers;
                end
                mData.lowMem = newMode;
            end
        end
        
        function mHead = saveobj(mData)
            % Returns a modelDataHeader object
            if ~mData.isMeta
                mHead = modelDataHeader(mData.simName,mData.modelName,...
                    mData.fileName,mData.lowMem);
                clear mData;
            else
                mHead = mData;
            end
        end
        
        function mData = modelData(ctmFile,varargin)
            % Use UI to find file if no path specified
            isDir = exist(ctmFile,'dir');
            if isempty(ctmFile) || isDir
                if isDir
                    initPath = ctmFile;
                else
                    initPath = 'ctm.bpch';
                end
                [tempFile,fileDir] = uigetfile('*.*','Select CTM file',initPath);
                if ~ischar(tempFile) && tempFile == 0
                    error('modelData:badFile','User must select a valid CTM output file');
                end
                ctmFile = fullfile(fileDir,tempFile);
            end
            
            mData.fileName = ctmFile;
            subPath = fileparts(ctmFile);
            tFileDefault = fullfile(subPath,'tracerinfo.dat');
            dFileDefault = fullfile(subPath,'diaginfo.dat');
            
            % Parse input arguments
            inParse = inputParser;
            inParse.addOptional('Verbose',      true,           @(x)validateattributes(x,{'logical'},{'scalar'}));
            inParse.addOptional('SimName',      'Unknown',      @(x)validateattributes(x,{'char'},{'vector'}));
            inParse.addOptional('ModelName',    'Unknown',      @(x)validateattributes(x,{'char'},{'vector'}));
            inParse.addOptional('ReadMetaData', true,           @(x)validateattributes(x,{'logical'},{'scalar'}));
            inParse.addOptional('ClobberMeta',  false,          @(x)validateattributes(x,{'logical'},{'scalar'}));
            inParse.addOptional('LowMem',       false,          @(x)validateattributes(x,{'logical'},{'scalar'}));
            inParse.addOptional('BruteForce',   false,          @(x)validateattributes(x,{'logical'},{'scalar'}));
            inParse.addOptional('DiagInfo',     dFileDefault,   @(x)validateattributes(x,{'char'},{'vector'}));
            inParse.addOptional('TracerInfo',   tFileDefault,   @(x)validateattributes(x,{'char'},{'vector'}));
            inParse.parse(varargin{:});
            mData.simName = inParse.Results.SimName;
            mData.modelName = inParse.Results.ModelName;
            readMetaData = inParse.Results.ReadMetaData;
            clobberMeta = inParse.Results.ClobberMeta;
            setLowMem = inParse.Results.LowMem;
            bruteForce = inParse.Results.BruteForce;
            verbose = inParse.Results.Verbose;
            mData.tracerFileName = inParse.Results.TracerInfo;
            mData.diagFileName = inParse.Results.DiagInfo;
            
            % Check that it's actually a file
            fileFields = {'tracer','','diag'};
            fileOK = false(length(fileFields),1);
            errFiles = {'tracerinfo','data','diaginfo'};
            for iFile = 1:length(fileFields)
                nameField = sprintf('%sFileName',fileFields{iFile});
                nameField(1) = lower(nameField(1));
                
                dateField = sprintf('%sFileDate',fileFields{iFile});
                dateField(1) = lower(dateField(1));
                
                currFile = mData.(nameField);
                fileOK(iFile) = ~isdir(currFile);
                if fileOK(iFile)
                   fileOK(iFile) = exist(currFile,'file');
                end
                
                if fileOK(iFile)
                    % Store last modified date for each of the three files
                    dirData = dir(currFile);
                    mData.(dateField) = dirData.datenum;
                end
            end
            if ~all(fileOK)
                errFormat = repmat('%s, ',[1,sum(~fileOK)]);
                errFormat = errFormat(1:(end-2));
                errFiles = errFiles(~fileOK);
                errList = sprintf(errFormat,errFiles{:});
                error('modelData:filesMissing','Could not find file(s): %s',errList);
            end
            
            % Open file
            ctmFileID = fopen(ctmFile,'r','ieee-be');
            %cleanupFn = onCleanup(@()fclose(ctmFileID));

            % Get header information
            [~,~,rOK_A] = readFORTRANRecord(ctmFileID,'*char',1);
            [titleLine,~,rOK_B] = readFORTRANRecord(ctmFileID,'*char',1);
            
            if ~(rOK_A && rOK_B)
                fclose(ctmFileID);
                error('modelData:badFile','Error reading ''%s''',ctmFile);
            elseif ~any(strcmpi(strtrim(titleLine'),...
                    'geos-chem binary punch file v. 2.0'))
                % Changed to a warning, as we still have some functionality
                %{ 
                fclose(ctmFileID);
                error('modelData:notCTM','Input file ''%s'' must be a ctm.bpch file.',...
                    ctmFile);
                %}
                if (~strncmpi(titleLine','CTM output saved by GAMAP',15)) && verbose
                    warning('modelData:notCTM','Input file ''%s'' is not a ctm.bpch file. Expect limited functionality.',...
                        ctmFile);
                end
            end
            
            [~,modelName,modelRes,halfPolarT,center180T]=...
                readFixedFORTRANRecord(ctmFileID,'*char',20,'*float32',2,'*int32',1,'*int32',1);
            modelName = strtrim(modelName');

            % Build the grid specified
            [pOffset,pFactor] = parseGridVert(parseGCModel(modelName));
            mData.gSpec = gridSpec(modelRes(1),modelRes(2),halfPolarT,...
                center180T,pOffset,pFactor);
            
            mData.metModel = modelName;
            mData.lonRes = modelRes(1);
            mData.latRes = modelRes(2);
            mData.halfPolar = halfPolarT;
            mData.center180 = center180T;
            
            % We can only find out the grid resolution - not necessarily
            % the size (e.g. if nested)
            %[latE,lonE,altE,mData.pEdge] = parseGridData(modelName,modelRes);
            %mData.nLat = length(latE) - 1;
            %mData.nLon = length(lonE) - 1;
            %mData.nLev = length(altE) - 1;
            mData.nLat = mData.gSpec.nLat;
            mData.nLon = mData.gSpec.nLon;
            mData.nLev = mData.gSpec.nLev;
            mData.latEdge = mData.gSpec.latEdge;
            mData.lonEdge = mData.gSpec.lonEdge;
            mData.altEdge = mData.gSpec.zEdge;
            fclose(ctmFileID);
            
            % Read in meta data
            if readMetaData
                mData.processFile(bruteForce,clobberMeta,verbose);
            end
            
            if setLowMem
                mData.lowMemMode(true);
            end
        end
        
        function mDataOut = reload(mDataIn,varargin)
            % Always reread the metadata
            mdlArg = union(varargin,'clobbermeta');
            mDataOut = modelData(mDataIn.simName,mDataIn.modelName,...
                mDataIn.fileName,true,'clobbermeta',mdlArg{:});
        end
        
        function getListen(mData,~,eventData)
            % tData: Tracer data data is being requested for
            % eventName: Should simply be `PreGet'
            newTrac = eventData.AffectedObject.tracID;
            newCat = eventData.AffectedObject.catID;
            if mData.lowMem && ~isempty(mData.lastTrac)
                % Clear last tracer accessed
                if ~(strcmpi(newTrac,mData.lastTrac) && ...
                        strcmpi(newCat,mData.lastCat))
                    mData.tracData.(mData.lastCat).(mData.lastTrac).data = [];
                end
            end
            if isempty(eventData.AffectedObject.data)
                % Read in data
                mData.readTracer(newTrac,newCat);
            end
            mData.lastTrac = eventData.AffectedObject.tracMini;
            mData.lastCat = eventData.AffectedObject.catMini;
        end
        
        function readAllData(mData)
            if ~mData.dataProc
                % Process information file
                mData.processFile;
            end
            catNames = fieldnames(mData.tracData);
            for iCat = 1:mData.nCat
                currCat = catNames{iCat};
                tracNames = fieldnames(mData.tracData.(currCat));
                nTrac = length(tracNames);
                for iTrac = 1:nTrac
                    % Ignore synthetic tracers
                    if ~isempty(mData.tracData.(currCat).(tracNames{iTrac}).fileData)
                        mData.readTracer(tracNames{iTrac},currCat);
                    end
                end
            end
            fprintf('Tracer data has been read into memory.\n');
        end
        
        function sumTracers(mData,newTrac,catID,tracID,tracMult)
            % Make new record for a composite tracer
            if ~strcmpi(catID,fieldnames(mData.tracData))
                error('sumTracers:badCat','Category ''%s'' does not exist.',catID);
            end
            nTrac = length(tracID);
            if nargin < 5
                tracMult = ones(nTrac,1);
            end
            % Establish new tracer
            for iTrac=1:nTrac
                currTrac = tracID{iTrac};
                if ~strcmpi(currTrac,fieldnames(mData.tracData.(catID)))
                    warning('sumTracers:badTrac','Tracer ''%s'' not found in category ''%s''.',currTrac,catID);
                else
                    if iTrac == 1
                        tData = tracMult(1).*(mData.tracData.(catID).(currTrac).copyTracer(false));
                    else
                        tData = tData + (tracMult(iTrac).*mData.tracData.(catID).(currTrac));
                    end
                end
            end
            % Clear the file data
            tData.fileData = struct([]);
            mData.tracData.(catID).(newTrac) = tData;
        end
        
        function processFile(mData,bruteForce,doClobber,verbose)
            if nargin < 3
                % Overwrite even if present?
                doClobber = false;
                % Ignore minor errors?
                if nargin < 2
                    bruteForce = false;
                end
            end
            if nargin < 4
                verbose = true;
            end
            % Is there stored information on the file?
            metaFile = sprintf('%s.meta.mat',mData.fileName);
            metaKnown = exist(metaFile,'file') && ~doClobber;
            if metaKnown
                % Check that it was built for the same data
                load(metaFile,'metaData');
                % Compatibility check
                versionCheck = any(strcmpi(fieldnames(metaData),'isMeta')); %#ok<NODEF>
                dateCheck = (metaData.fileDate == mData.fileDate) && ...
                    (metaData.tracerFileDate == mData.tracerFileDate) && ...
                    (metaData.diagFileDate == mData.diagFileDate);
                metaKnown = versionCheck && dateCheck;
                % Was this necessary?
                %{
                if ~metaKnown
                    clear metaData;
                end
                %}
            end
            if metaKnown
                % Copy data straight over
                catNames = fieldnames(metaData.tracData);
                nReadReport = 0;
                for iCat = 1:metaData.nCat
                    currCat = catNames{iCat};
                    tracNames = fieldnames(metaData.tracData.(currCat));
                    nTrac = length(tracNames);
                    for iTrac = 1:nTrac
                        currTrac = tracNames{iTrac};
                        tData = metaData.tracData.(currCat).(currTrac).copyTracer(true);
                        addlistener(tData,'data','PreGet',@mData.getListen);
                        mData.tracData.(currCat).(currTrac) = tData;
                        nReadReport = nReadReport + 1;
                    end
                end
                mData.tEdge = tData.tEdge;
                mData.tVec = tData.tVec;
                mData.nMeta = metaData.nMeta;
                mData.nCat = metaData.nCat;
                mData.isMeta = false;
                if verbose
                    %fprintf('Metadata retrieved for %i tracers in %i categories.\n',...
%                        metaData.nMeta,metaData.nCat);
                    fprintf('Metadata retrieved for %i tracers in %i categories (%s to %s).\n',...
                        nReadReport,metaData.nCat,datestr(metaData.tEdge(1),'yyyy-mmm-dd HH:MM'),...
                        datestr(metaData.tEdge(end),'yyyy-mmm-dd HH:MM'));
                end
            else
                % Create new model data file which will contain only
                % metadata
                metaData = modelData(mData.fileName,'readMetaData',false,...
                    'simName',mData.simName,'modelName',mData.modelName,...
                    'verbose',verbose);
                
                % Determine locations of all tracer blocks in the file
                tracerInfo = mData.tracerFileName;
                diagInfo = mData.diagFileName;
                metaData.tracerFileName = mData.tracerFileName;
                metaData.diagFileName = mData.diagFileName;
                metaData.tracerFileDate = mData.tracerFileDate;
                metaData.diagFileDate = mData.diagFileDate;

                % Open the file
                ctmFileID = fopen(mData.fileName,'r','ieee-be');

                % Skip header information (read in earlier)
                readFORTRANRecord(ctmFileID,'seekpast');
                readFORTRANRecord(ctmFileID,'seekpast');

                %% Read in data to establish main data structure
                dataStartPos = ftell(ctmFileID);

                % First line of the header of the first datablock
                readFORTRANRecord(ctmFileID,'seekpast');

                % Read tracer database
                [tID,tName,tWeight,~,tNum,tScale,tUnit] = readTracerData(tracerInfo);

                % This is the total number of entries we will require
                % Should set up a new tracerData for each
                numTracers = length(tID);

                % Read diagnostics information
                [dOffset,dName,dFull] = readDiagInfo(diagInfo);
                numDiags = length(dOffset);

                %% Sanitise field names
                % Some diagnostics and tracer have names that won't work as MATLAB fields
                % Convert these to safe strings for field names
                dNameSafe = dName;
                for iDiag = 1:numDiags
                    safeStr = char(dName{iDiag});
                    if ~(isempty(regexp(safeStr,'-\$', 'once')) && isempty(regexp(safeStr,'=\$', 'once')))
                        % Last two characters are -$ or =$
                        safeStr = safeStr(1:end-2);
                    end
                    safeStr(regexp(safeStr,'\$')) = '';
                    safeStr(regexp(safeStr,')')) = '';
                    safeStr(regexp(safeStr,'(')) = '';
                    safeStr(regexp(safeStr,'-')) = '_';
                    safeStr(regexp(safeStr,' ')) = '_';
                    % Ensure that the start of the category name is valid
                    safeStr = ['C_' safeStr]; %#ok<AGROW>
                    if ~isvarname(safeStr)
                        if ~bruteForce
                            fclose(ctmFileID);
                            error('BPCHRead:FieldSanity','Could not produce safe field name for diagnostic category %s (attempted ''%s'').',char(dName{iDiag}),safeStr);
                        end
                    end
                    dNameSafe(iDiag) = {safeStr};
                end
                tIDSafe = tID;
                for iTracer = 1:numTracers
                    safeStr = char(tID{iTracer});
                    safeStr(regexp(safeStr,'\$')) = '';
                    safeStr(regexp(safeStr,')')) = '';
                    safeStr(regexp(safeStr,'(')) = '';
                    safeStr(regexp(safeStr,'-')) = '_';
                    safeStr(regexp(safeStr,' ')) = '_';
                    % Ensure that the start of the category name is valid
                    safeStr = ['T_' safeStr]; %#ok<AGROW>
                    if ~isvarname(safeStr)
                        if ~bruteForce
                            fclose(ctmFileID);
                            error('BPCHRead:FieldSanity','Could not produce safe field name for tracer %s (attempted ''%s'').',char(tID{iTracer}),safeStr);
                        end
                    end
                    tIDSafe(iTracer) = {safeStr};
                end

                % Rewind to start of data (returns 0 if successful, -1 otherwise)
                if fseek(ctmFileID,dataStartPos,-1)
                    fclose(ctmFileID);
                    error('Could not rewind to start of data blocks.');
                end
                fileComplete = false;

                % First pass - determine length of data
                % Assume no more than 1e6 pieces of data
                %trcFound = false(numTracers,1);
                % Inefficient - quick fix
                trcFound = false(numTracers,numDiags);
                currLoc = ftell(ctmFileID);
                fseek(ctmFileID,0,1);
                endLoc = ftell(ctmFileID);
                fseek(ctmFileID,currLoc,-1);
                nReadReport = 0;

                if verbose
                    %{
                    fprintf('Last date processed: %s',...
                        datestr(0,'yyyymmdd HH:MM'));
                    %}
                    dFormat = 'yyyy-mm-dd HH:MM';
                    fprintf('Reading metadata for ''%s''\nLast period processed: ',mData.fileName);
                    lastMsg = sprintf('%s to %s',...
                        datestr(0,dFormat),...
                        datestr(1,dFormat));
                    fprintf('%s',lastMsg);
                    lastTau = 1;
                end
                while ~fileComplete
                    % Header lines - formatting taken direct from website
                    % First line: model name, model resolution
                    [~,mdlName,mdlRes,halfPolarT,center180T]...
                        =readFixedFORTRANRecord(ctmFileID,'*char',20,'*float32',2,'*int32',1,'*int32',1);
                    % Build the grid specified
                    mdlName = strtrim(mdlName');
                    isOK = true;
                    try
                        [pOffset,pFactor] = parseGridVert(parseGCModel(mdlName));
                    catch ME
                        warning('modelData:badFile','File read failed');
                        isOK = false;
                    end
                    if isOK
                        gSpecT = gridSpec(mdlRes(1),mdlRes(2),halfPolarT,...
                            center180T,pOffset,pFactor);
                        % Second line: information about the tracer itself.
                        %   Diagnostic category name
                        %   Tracer number
                        %   Unit string (e.g. ppbv)
                        %   Starting date (tau value - hours since 1/1/1985)
                        %   Ending date
                        %   Unused string
                        %   Dimensions - NI, NJ, NL, I0, J0, L0
                        %   Length of data block in bytes

                        [readOK,diagCat,tracer,trcUnit,startTau,endTau,~,readDims,~]...
                            =readFixedFORTRANRecord(ctmFileID,'*char',40,'*int32',1,'*char',40,...
                            '*float64',1,'*float64',1,'*char',40,'*int32',6,'*int32',1);
                    else
                        readOK = false;
                    end

                    if readOK
                        diagCat = strtrim(diagCat');
                        catIndex = find(strcmp(diagCat,dName));
                        readOK = ~isempty(dOffset(catIndex));
                    end
                    
                    if ~(bruteForce || readOK)
                        % Stop reading and return partial data
                        fileComplete = true;
                        warning('readBPCHSingle:partialRead','File corrupt or incomplete; partial data returned.');
                    else
                        tracer = tracer + dOffset(catIndex);
                        tracIndex = find(tNum==tracer);
                        if ~(isempty(tracIndex) || isempty(catIndex))
                            % Set up tracer if necessary
                            storeCat = dNameSafe{catIndex}(3:end);
                            %storeTrac = tIDSafe{tracIndex}(3:end);
                            storeTrac = matlab.lang.makeValidName(tIDSafe{tracIndex}(3:end));
                            if ~trcFound(tracIndex,catIndex)
                                nReadReport = nReadReport + 1;
                                trcFound(tracIndex,catIndex) = true;
                                tData = tracerData(storeTrac,storeCat);
                                tData.nSamples = 0;
                                tData.tVec = zeros(1e4,1);
                                tData.tEdge = zeros(1e4+1,1);
                                %{
                                [latE,lonE,altE,pE] = ...
                                    parseGridData(mdlName,mdlRes);
                                %}
                                latE = gSpecT.latEdge;
                                lonE = gSpecT.lonEdge;
                                altE = gSpecT.zEdge;
                                pE = gSpecT.pEdge;
                                % Tracer may not occupy full grid
                                tData.lonEdge = lonE(readDims(4)+(0:readDims(1)));
                                tData.latEdge = latE(readDims(5)+(0:readDims(2)));
                                % Some tracers are defined on level edges 
                                maxLev = readDims(6) + readDims(3) - 1;
                                if maxLev == length(altE)
                                    % Assume that the grid is defined at
                                    % pressure level edges
                                    tData.gridCells = false;
                                    tData.altEdge = altE;
                                    tData.pEdge = pE;
                                else
                                    altMax = min(readDims(3)+readDims(6),...
                                        length(altE))-readDims(6);
                                    tData.altEdge = altE(readDims(6)+(0:altMax));
                                    tData.pEdge = pE(readDims(6)+(0:altMax));
                                end
                                tData.fileData.fileName = mData.fileName;
                                tData.fileData.readLocs = zeros(1e4,1,'uint64');
                                tData.tracID = ['T_' storeTrac];
                                tData.catID = ['C_' storeCat];
                                tData.tracName = tName{tracIndex};
                                tData.catName = dFull{catIndex};
                                tData.internalUnit = strtrim(trcUnit');
                                tData.unit = tUnit{tracIndex};
                                tData.unitScale = tScale(tracIndex);
                                tData.molMass = tWeight(tracIndex);
                                metaData.tracData.(storeCat).(storeTrac) = tData.copyTracer(true);
                                addlistener(tData,'data','PreGet',@mData.getListen);
                                mData.tracData.(storeCat).(storeTrac) = tData;
                            end
                            try
                                iSample = mData.tracData.(storeCat).(storeTrac).nSamples + 1;
                            catch ME
                                rethrow(ME);
                            end
                            mData.tracData.(storeCat).(storeTrac).nSamples = iSample;
                            
                            startTau = (startTau/24) + datenum(1985,1,1,0,0,0);
                            endTau = (endTau/24) + datenum(1985,1,1,0,0,0);
                            if iSample == 1
                                mData.tracData.(storeCat).(storeTrac).tEdge(iSample) = startTau;
                            end
                            mData.tracData.(storeCat).(storeTrac).tEdge(iSample+1) = endTau;
                            meanTau = (startTau + endTau)/2;
                            mData.tracData.(storeCat).(storeTrac).tVec(iSample) = meanTau;
                            currLoc = ftell(ctmFileID);
                            mData.tracData.(storeCat).(storeTrac).fileData.readLocs(iSample) = currLoc;
                            if verbose && (lastTau ~= endTau)
                                %{
                                fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b%s',...
                                    datestr(meanTau,'yyyymmdd HH:MM'));
                                fprintf('%20.10f\n',meanTau);
                                %}
                                dTau = endTau - startTau;
                                dTauRound = round(dTau);
                                if dTauRound == 365 || dTauRound == 366
                                    dFormat = 'yyyy';
                                elseif dTauRound > 25 && dTauRound < 35
                                    dFormat = 'yyyy-mmm';
                                elseif dTauRound == 1
                                    dFormat = 'yyyy-mm-dd';
                                else
                                    dFormat = 'yyyy-mm-dd HH:MM';
                                end
                                fprintf(repmat('\b',[1,length(lastMsg)]));
                                if endTau == startTau
                                    lastMsg = sprintf('%s',...
                                        datestr(endTau,dFormat));
                                else
                                    lastMsg = sprintf('%s to %s',...
                                        datestr(startTau,dFormat),...
                                        datestr(endTau,dFormat));
                                end
                                fprintf('%s',lastMsg);
                                lastTau = endTau;
                            end
                        end
                        % Skip actual data
                        readFORTRANRecord(ctmFileID,'seekpast');
                        currLoc = ftell(ctmFileID);
                        locDelta = endLoc - currLoc;
                        if locDelta == 0
                            fileComplete = true;
                        end
                    end
                end

                % Strip down the tracer read locations to just the number of
                % samples identified
                storedCat = fieldnames(mData.tracData);
                nCategories = length(storedCat);
                for iCat = 1:nCategories
                    currCat = storedCat{iCat};
                    storedTrac = fieldnames(mData.tracData.(currCat));
                    nTrac = length(storedTrac);
                    for iTrac = 1:nTrac
                        currTrac = storedTrac{iTrac};
                        currSamples = mData.tracData.(currCat).(currTrac).nSamples;
                        mData.tracData.(currCat).(currTrac).fileData.readLocs = ...
                            mData.tracData.(currCat).(currTrac).fileData.readLocs(1:currSamples);
                        mData.tracData.(currCat).(currTrac).tVec = ...
                            mData.tracData.(currCat).(currTrac).tVec(1:currSamples);
                        mData.tracData.(currCat).(currTrac).tEdge = ...
                            mData.tracData.(currCat).(currTrac).tEdge(1:(currSamples+1));
                        % Store metadata too
                        metaData.tracData.(currCat).(currTrac).fileData.readLocs = ...
                            mData.tracData.(currCat).(currTrac).fileData.readLocs(1:currSamples);
                        metaData.tracData.(currCat).(currTrac).tVec = ...
                            mData.tracData.(currCat).(currTrac).tVec(1:currSamples);
                        metaData.tracData.(currCat).(currTrac).tEdge = ...
                            mData.tracData.(currCat).(currTrac).tEdge(1:(currSamples+1));
                        metaData.tracData.(currCat).(currTrac).nSamples = currSamples;
                    end
                end
                mData.nMeta = numTracers;
                mData.nCat = nCategories;
                metaData.nMeta = numTracers;
                metaData.nCat = nCategories;
                
                % Also store timing data from the last tracer (should all
                % be identical)
                metaData.tEdge = metaData.tracData.(currCat).(currTrac).tEdge;
                metaData.tVec = metaData.tracData.(currCat).(currTrac).tVec;
                mData.tEdge = metaData.tracData.(currCat).(currTrac).tEdge;
                mData.tVec = metaData.tracData.(currCat).(currTrac).tVec;
                
                % Store metadata externally
                metaData.isMeta = true;
                %{
                storeMeta = true;
                if storeMeta
                    save(metaFile,'metaData');
                else
                    warning('modelData:noMeta','Metadata storage disabled!');
                end
                %}
                save(metaFile,'metaData');
                fclose(ctmFileID);
                
                if verbose
                    fprintf('\n');

                    fprintf('Metadata stored for %i tracers in %i categories (%s to %s).\n',...
                        nReadReport,nCategories,datestr(metaData.tEdge(1),'yyyy-mmm-dd HH:MM'),...
                        datestr(metaData.tEdge(end),'yyyy-mmm-dd HH:MM'));
                end
            end
            mData.dataProc = true;
        end
        
        function disp(mData)
            fprintf('%s ''%s'' simulation data.\n',mData.modelName,mData.simName);
            if ~isempty(mData.startDate)
                fprintf('Handle for data on %i tracers in %i categories.\n',mData.nMeta,mData.nCat);
                fprintf('\tSource file:\t\t%s\n',mData.fileName);
                tStart = datestr(datenum(mData.startDate,'yyyymmdd'),'yyyy mmm dd');
                tEnd = datestr(datenum(mData.endDate,'yyyymmdd'),'yyyy mmm dd');
                fprintf('\tSimulation start:\t%s\n',tStart);
                fprintf('\tSimulation end:\t\t%s\n',tEnd);
                fprintf('\tHorizontal resolution:\t%ix%i\n',mData.latRes,mData.lonRes);
                fprintf('\tPressure levels:\t%i\n',mData.nLev);
                gridLims = [min(mData.latEdge),max(mData.latEdge)];
                gridStr = 'NN';
                gridStr(gridLims<0) = 'S';
                gridLims = abs(gridLims);
                fprintf('\tLatitudinal range:\t%4.2f%s - %4.2f%s\n',gridLims(1),gridStr(1),gridLims(2),gridStr(2));
                gridLims = [min(mData.lonEdge),max(mData.lonEdge)];
                gridStr = 'EE';
                gridStr(gridLims<0) = 'W';
                gridLims = abs(gridLims);
                fprintf('\tLongitudinal range:\t%4.2f%s - %4.2f%s\n',gridLims(1),gridStr(1),gridLims(2),gridStr(2));
                fprintf('\tModel top:\t\t%4.2f hPa\n',min(mData.pEdge));
            end
        end
        
        function [hFig,hAxVec,hPlotVec] = plotZonal(mData,tracName,catName,...
                plotDate,maxLev)
            tData = mData.tracData.(catName).(tracName);
            if nargin < 5
                maxLev = tData.nLev;
            end
            if nargin > 1
                if ischar(plotDate)
                    iSample = tData.parseDate(plotDate);
                    plotData = mData.readSubRange(tracName,catName,iSample);
                    plotString = sprintf('%s %s %s',mData.simName,tData.tracMini,...
                        datestr(tData.tVec(iSample),'yymmdd-HHMM'));
                else
                    % Multiple dates given, either as vector of datenums or
                    % as cell array of strings; take mean
                    nDates = length(plotDate);
                    sampleVec = zeros(nDates,1);
                    if iscell(plotDate)
                        for iDate = 1:nDates
                            sampleVec(iDate) = tData.parseDate(plotDate{iDate});
                        end
                    else
                        % Another possibility - using sample numbers
                        sampleOK = false;
                        if max(plotDate) < min(tData.tEdge(1)-50)
                            if all(plotDate>=1) && all(plotDate<=length(tData.tVec))
                                sampleVec = plotDate;
                                sampleOK = true;
                            end
                        end
                        if ~sampleOK
                            for iDate = 1:nDates
                                sampleVec(iDate) = tData.parseDate(plotDate(iDate));
                            end
                        end
                    end
                    plotData = mean(mData.readSubRange(tracName,catName,sampleVec),4);
                    plotString = sprintf('%s %s %s',mData.simName,tData.tracMini,...
                        datestr(mean(tData.tVec(sampleVec)),'yymmdd-HHMM'));
                end
            elseif tData.nSamples == 1
                % Unambiguous, really
                plotData = tData.data(:,:,:,1);
                plotString = sprintf('%s %s',tData.tracMini,...
                    datestr(tData.tVec,'yymmdd-HHMM'));
            else
                error('plotZonal:badArg','No target date given.');
            end

            if nargin < 3
                maxLev = tData.nLev;
            end
            
            [hFig,hAxVec,hPlotVec] = plotGrid(plotData(:,:,1:maxLev),'zonal',...
                'latedge',tData.latEdge,'gridcells',tData.gridCells,'altedge',...
                tData.altEdge(1:maxLev+tData.gridCells),'unit',tData.ltxUnit);
            set(hFig,'name',plotString);
            %{
            plotGridData(plotData,1,0,...
                false,tData.ltxUnit,'latvals',tData.latEdge,'lonvals',...
                tData.lonEdge,'altvals',tData.altEdge);
            %}
        end
        
        function [hPlot] = plotLayer(mData,tracName,catName,targLayer,plotDate)
            tData = mData.tracData.(catName).(tracName);
            if nargin < 5
                if tData.nSamples > 1
                    error('plotLayer:noDate','Need target date for plot');
                else
                    iSample = 1;
                end
            elseif ischar(plotDate)
                iSample = tData.parseDate(plotDate);
            elseif iscell(plotDate)
                if length(plotDate) > 1 || ~ischar(plotDate{1})
                    error('plotLayer:badCell','Need unique target date for plot');
                end
                iSample = tData.parseDate(plotDate);
            else
                % Using sample numbers or direct date numbers
                sampleOK = false;
                if plotDate < min(tData.tEdge(1)-50)
                    if plotDate>=1 && plotDate<=length(tData.tVec)
                        iSample = plotDate;
                        sampleOK = true;
                    end
                end
                if ~sampleOK
                    iSample = tData.parseDate(plotDate(1));
                end
            end
            % Allow for column totals
            if targLayer == 0
                dataIn = mData.readSubRange(tracName,catName,iSample);
                dataIn = transpose(sum(dataIn,3));
                targLayer = 1;
                titleString = sprintf('%sTotal',tData.tracMini);
            else
                dataIn = mData.readSubRange(tracName,catName,iSample,{':',':',targLayer});
                titleString = sprintf('%s %4.2f km %s',tData.tracMini,tData.altCentre(targLayer),...
                    datestr(mData.tracData.(catName).(tracName).tVec(iSample),'yyyymmdd'));
                %if tData.nLev == 1
                    % Need to transpose
                    dataIn = dataIn';
                %end
            end

            hPlot = plotGrid(dataIn,'layer',...
                'ilayer',targLayer,'plotUnit',tData.ltxUnit,...
                'latvals',tData.latEdge,'lonvals',tData.lonEdge);
            set(gcf,'name',titleString);
        end
        
        function [lineData,hLine,hPlot,hFig] = plotColumn(...
                mData,catName,tracList,targDate,targLon,targLat,varargin)
            % Plot vertical profiles of one or more tracers
            nArg = length(varargin);
            iArg = 1;
            tracMult = ones(size(tracList));
            while iArg <= nArg
                currArg = varargin{iArg};
                switch lower(currArg)
                    case 'tracmult'
                        tracMult = varargin{iArg+1};
                        nPlus = 2;
                    otherwise
                        warning('plotColumn:badArg',...
                            'Argument ''%s'' not recognized.',currArg);
                        nPlus = 1;
                end
                iArg = iArg + nPlus;
            end
            % Get data from first tracer
            sampleTrac = mData.tracData.(catName).(tracList{1});
            altCentre = sampleTrac.altCentre;
            nAlt = sampleTrac.nLev;
            targSample = sampleTrac.parseDate(targDate);
            nTrac = length(tracList);
            % Set up plot
            hFig = figure('color',[1 1 1],'windowstyle','docked');
            hPlot = axes('position',[0.1 0.1 0.8 0.8]);
            tracNames = cell(nTrac,1);
            hLine = zeros(nTrac,1);
            lineData = zeros(nTrac,nAlt);
            lineStyleList = {'-','-.','--'};
            % Do we need to area weight?
            areaWeight = length(targLat)>1;
            if areaWeight
                gridWeight = sampleTrac.gridArea;
                gridWeight = gridWeight(targLon(1):targLon(end),...
                    targLat(1):targLat(end));
                gridSum = sum(gridWeight(:));
                gridWeight = repmat(gridWeight,[1 1 sampleTrac.nLev]);
            end
            % Determine if we are in low memory mode
            runLean = mData.lowMem;
            for iTrac=1:nTrac
                if nTrac > 1
                    cVar = (iTrac-1)/(nTrac-1);
                    lineCol = [cVar,0,(1-cVar)];
                    iStyle = 1 + mod(iTrac-1,length(lineStyleList));
                    lineStyle = lineStyleList{iStyle};
                else
                    lineCol = [0 0 1];
                    lineStyle = lineStyleList{1};
                end
                currTrac = mData.tracData.(catName).(tracList{iTrac});
                if runLean
                    [~,~,currData] = mData.readSubRange(...
                        tracList{iTrac},catName,sampleTrac.tEdge(...
                        [targSample, targSample+1]));
                    currData = currData(targLon,targLat,:);
                else
                    currData = currTrac.data(targLon,targLat,:,targSample);
                end
                if ~areaWeight
                    lineData(iTrac,:) = tracMult(iTrac).*...
                        squeeze(mean(currData,1));
                else
                    tempLine = sum(sum(currData.*gridWeight,1),2);
                    tempLine = tracMult(iTrac).*squeeze(tempLine)./gridSum;
                    lineData(iTrac,:) = tracMult(iTrac).*tempLine;
                end
                hLine(iTrac) = line('XData',lineData(iTrac,:),'YData',...
                    altCentre,'linestyle',lineStyle,'color',lineCol,...
                    'linewidth',2);
                tracNames(iTrac) = {currTrac.ltxName};
            end
            legend(hPlot,tracNames,'interpreter','latex','fontsize',24);
            set(hPlot,'fontsize',20,'XGrid','on','YGrid','on',...
                'YLim',[0,max(altCentre)]);
            ylabel(hPlot,'Altitude (km)','fontsize',24,'interpreter','latex');
            xlabel(hPlot,'Abundance (ppbv)','fontsize',24,'interpreter','latex');
        end
        
        function clearTracer(mData,tracName,catName)
            % Clear data from memory, leave empty tracer with metadata
            if nargin > 2
                if iscell(catName)
                    if length(catName) > 1
                        error('modelData:manyCategories','Can only read one category at a time.');
                    else
                        catName = catName{1};
                    end
                end
            else
                % Assume C_IJ_24H
                catName = 'C_IJ_24H';
            end

            if length(catName) > 2 && strcmpi(catName(1:2),'C_')
                storeCat = catName(3:end);
            else
                storeCat = catName;
                %catName = sprintf('C_%s',storeCat);
            end

            if ~iscell(tracName)
                tracName = {tracName};
            end

            nTrac = length(tracName);
            
            for iTrac = 1:nTrac
                mData.tracData.(storeCat).(storeTrac).data = [];
            end
        end
        
        function [varargout] = readSubRange(mData,tracName,catName,tEdge,xyzRange,readRaw)
            % Specialized function to read only a data subrange for 1
            % tracer at a time
            if nargin > 2
                if iscell(catName)
                    if length(catName) > 1
                        error('modelData:manyCategories','Can only read one category at a time.');
                    else
                        catName = catName{1};
                    end
                end
            else
                % Assume IJ_24H
                catName = 'IJ_24H';
            end
            
            if nargin < 6
                readRaw = false;
            end
            
            if strncmpi(catName,'C_',2)
                storeCat = catName(3:end);
            else
                storeCat = catName;
            end

            if mData.dataProc
                ctmFileID = fopen(mData.fileName,'r','ieee-be');
            end
            
            if iscell(tracName)
                tracName = tracName{1};
            end
            
            if length(tracName) > 2 && strcmpi(tracName(1:2),'T_')
                storeTrac = tracName(3:end);
            else
                storeTrac = tracName;
            end
            % If file is preprocessed, use metadata to speed things up
            if mData.dataProc
                % Now read in real data
                tracGrid = mData.tracData.(storeCat).(storeTrac).gridDims;
                % How much do we actually want to return?
                outGrid = tracGrid;
                if nargin < 5 || isempty(xyzRange)
                    xyzRange = cell(length(tracGrid)-1,1);
                    xyzRange(:) = {':'};
                else
                    % Only want a subrange in spatial co-ords
                    if ~iscell(xyzRange)
                        oldRange = xyzRange;
                        xyzRange = cell(length(tracGrid)-1,1);
                        for iDim = 1:length(oldRange)
                            xyzRange{iDim} = oldRange(iDim);
                        end
                    end
                    isOK = strcmpi(xyzRange,':');
                    for iDim = 1:length(xyzRange)
                        if ~isOK(iDim)
                            outGrid(iDim) = length(xyzRange{iDim});
                        end
                    end
                end
                tVecTrac = mData.tracData.(storeCat).(storeTrac).tVec;
                if nargin > 3 && ~isempty(tEdge)
                    % Specific time subrange requested
                    % Could it be samples only that were requested?
                    if max(tEdge) < min(mData.tracData.(storeCat).(storeTrac).tEdge) - 50 && ...
                        max(tEdge) <= length(tVecTrac) && min(tEdge) >= 1
                        % Actually requested a sample list
                        targSamples = false(size(tVecTrac));
                        targSamples(tEdge(:)) = true;
                    else
                        targSamples = tVecTrac>min(tEdge) & tVecTrac<max(tEdge);
                    end
                    tracGrid(end) = sum(targSamples);
                    outGrid(end) = tracGrid(end);
                else
                    targSamples = true(outGrid(end),1);
                end
                tVec = tVecTrac(targSamples);
                tEdge = mData.tracData.(storeCat).(storeTrac).tEdge;
                targEdge = false(size(tEdge));
                lastSample = find(targSamples,1,'last');
                targEdge(1:end-1) = targSamples;
                targEdge(lastSample+1) = true;
                
                if length(tVecTrac) == 1
                    targSamples = 1;
                else
                    targSamples = find(targSamples);
                    tEdge = tEdge(targEdge);
                end
                if isempty(targSamples)
                    warning('modelData:badRange','Target range exceeds data range.');
                    dataGrid = [];
                else
                    numDims = length(tracGrid) - 1;
                    readLocs = mData.tracData.(storeCat).(storeTrac).fileData.readLocs;
                    dataBlock = zeros(outGrid,mData.dType);
                    tracSpatial = tracGrid(1:3);
                    indexArray = cell(1,numDims+1);
                    indexArray(1:numDims) = {':'};
                    if readRaw
                        dataScale = 1;
                    else
                        dataScale = mData.tracData.(storeCat).(storeTrac).unitScale;
                    end
                    iStore = 0;
                    for iEntry = targSamples(1):targSamples(end)
                        iStore = iStore + 1;
                        fseek(ctmFileID,readLocs(iEntry),-1);
                        indexArray(end) = {iStore};
                        fullBlock = readFORTRANRecord(ctmFileID,'*float32',4);
                        tempSpatial = tracSpatial;
                        nPts = numel(fullBlock);
                        if nPts < prod(tracSpatial)
                            % Subrange?
                            nLevTemp = nPts/prod(tracSpatial(1:2));
                            if floor(nLevTemp)~=nLevTemp
                                error('modelData:badSize','Could not read data block');
                            end
                            tempBlock = fullBlock;
                            fullBlock = nan(tracSpatial);
                            fullBlock(:,:,1:nLevTemp) = tempBlock;
                            clear tempBlock;
                        else
                            fullBlock = reshape(fullBlock,tempSpatial);
                        end
                        % Retrieve only what we need
                        dataBlock(indexArray{:}) = fullBlock(xyzRange{:});
                    end
                    dataGrid = dataScale.*dataBlock;
                end
            else
                error('readSubRange:noMetaData',...
                    'Must read metadata before attempting to acquire a data subrange.');
            end
            fclose(ctmFileID);
            if nargout == 1
                varargout = {dataGrid};
            else
                varargout = {tEdge,tVec,dataGrid};
            end
        end
        
        function [dataBlock] = readAverage(mData,tracName,catName,tLims)
            % Reads time-averaged value
            subEdge = mData.tEdge;
            % Perform no interpolation
            subEdge = subEdge(subEdge >= tLims(1) & subEdge <= tLims(end));
            hiEdge = subEdge(1);
            for iSample = 1:(length(subEdge)-1)
                loEdge = hiEdge;
                hiEdge = subEdge(iSample+1);
                tDiff = hiEdge - loEdge;
                miniData = tDiff.*mData.readSubRange(tracName,catName,[loEdge hiEdge]);
                if iSample == 1
                    dataBlock = zeros(size(miniData),mData.dType);
                end
                dataBlock = dataBlock + miniData;
            end
            dataBlock = dataBlock./(subEdge(end)-subEdge(1));
        end
        
        function readTracer(mData,tracName,catName)
            % Adjust start and end dates
            if ~isempty(mData.startDate)
                mDataStart = datenum(mData.startDate,'yyyymmdd');
                mDataEnd = datenum(mData.endDate,'yyyymmdd');
            else
                mDataStart = +Inf;
                mDataEnd = -Inf;
            end
            if nargin > 2
                if iscell(catName)
                    if length(catName) > 1
                        error('modelData:manyCategories','Can only read one category at a time.');
                    else
                        catName = catName{1};
                    end
                end
            else
                % Assume C_IJ_24H
                catName = 'C_IJ_24H';
            end

            if length(catName) > 2 && strcmpi(catName(1:2),'C_')
                storeCat = catName(3:end);
            else
                storeCat = catName;
                %catName = sprintf('C_%s',storeCat);
            end

            if ~iscell(tracName)
                tracName = {tracName};
            end

            nTrac = length(tracName);

            if mData.dataProc
                ctmFileID = fopen(mData.fileName,'r','ieee-be');
            end
            
            for iTrac = 1:nTrac
                currTrac = tracName{iTrac};
                if length(currTrac) > 2 && strcmpi(currTrac(1:2),'T_')
                    storeTrac = currTrac(3:end);
                else
                    storeTrac = currTrac;
                end
                % If file is preprocessed, use metadata to speed things up
                if mData.dataProc
                    % Now read in real data
                    tracGrid = mData.tracData.(storeCat).(storeTrac).gridDims;
                    numDims = length(tracGrid) - 1;
                    readLocs = mData.tracData.(storeCat).(storeTrac).fileData.readLocs;
                    dataBlock = zeros(tracGrid,mData.dType);
                    tracSpatial = tracGrid(1:3);
                    indexArray = cell(1,numDims+1);
                    indexArray(1:numDims) = {':'};
                    dataScale = mData.tracData.(storeCat).(storeTrac).unitScale;
                    if dataScale == 0
                        % Bloody RADMAP
                        dataScale = 1;
                    end
                    for iEntry = 1:tracGrid(4)
                        fseek(ctmFileID,readLocs(iEntry),-1);
                        indexArray(end) = {iEntry};
                        dataBlock(indexArray{:}) = reshape(readFORTRANRecord(ctmFileID,'*float32',4),tracSpatial);
                    end
                    mData.tracData.(storeCat).(storeTrac).data = dataScale.*dataBlock;
                else
                    mData.tracData.(storeCat).(storeTrac) = ...
                        tracerData(storeTrac,storeCat,mData.fileName);
                end
                tracStart = min(mData.tracData.(storeCat).(storeTrac).tVec);
                tracEnd = max(mData.tracData.(storeCat).(storeTrac).tVec);
                if tracStart < mDataStart
                    mDataStart = tracStart;
                end
                if tracEnd > mDataEnd
                    mDataEnd = tracEnd;
                end
            end
            fclose(ctmFileID);
            mData.startDate = datestr(mDataStart,'yyyymmdd');
            mData.endDate = datestr(mDataEnd,'yyyymmdd');
        end
        
        function [iLon,iLat] = translateLoc(mData,xLon,yLat)
            % Translate longitude/latitude values into a grid cell index
            iLon = find(xLon<mData.lonEdge,1,'first')-1;
            iLat = find(yLat<mData.latEdge,1,'first')-1;
        end
        
        function [ atmMass,tropMass,stratMass ] = calcAtmMass( mData,tracList,catName,nSamples )
        %CALCATMMASS Calculate total atmospheric mass of a tracer
        %   Detailed explanation goes here
        if (ischar(tracList) && strcmpi(tracList,'all')) || isempty(tracList)
            tracList = fieldnames(mData.tracData.(catName));
        elseif ischar(tracList)
            tracList = {tracList};
        end
        nTrac = length(tracList);
        sampleTData = mData.tracData.(catName).(tracList{1});
        if nargin < 4
            nSamples = length(sampleTData.tVec);
        end
        atmMass = zeros(nSamples,nTrac,mData.dType);
        tropMass = atmMass;
        currMet = mData.metModel;
        if length(currMet) > 5
            metModelShort = currMet(1:5);
        else
            metModelShort = currMet;
        end
        switch upper(metModelShort)
            case 'GEOS5'
                metCommands = {{'I6','A3'},{'PS','TROPP'},'match'};
            case 'MERRA'
                metCommands = {{'I6','A1'},{'PS','TROPPT'},'match','metmodel','MERRA'};
            otherwise
                error('calcAtmMass:unknownMet','Met model ''%s'' unknown',currMet);
        end
        for iSample = 1:nSamples
            tLims = sampleTData.tEdge(iSample:iSample+1);
            [PSurf,PTrop] = readMetSeries(tLims,metCommands{:});
            % hPa
            [PEdge,PCenter] = calcPGrid(PSurf,false);
            % kg air/box
            airMass = calcAirMass(PEdge,sampleTData.gridArea);
            % kg tracer/box
            % Create a tropospheric mask
            lastTrop = 1;
            isTrop = false(size(airMass));
            for iLayer = 1:size(PCenter,3)
                % Stop once above the troposphere
                if lastTrop > 0
                    isTropLayer = PCenter(:,:,iLayer) > PTrop;
                    %tropMass(iSample,iTrac) = tropMass(iSample,iTrac) + sum(tempLayer(isTrop));
                    lastTrop = sum(isTropLayer(:));
                    isTrop(:,:,iLayer) = isTropLayer;
                end
            end
            for iTrac = 1:nTrac
                tracName = tracList{iTrac};
                tData = mData.tracData.(catName).(tracName);
                tempData = mData.readSubRange(tracName,catName,iSample);
                % kg/mol
                molMass = tData.molMass;
                molMassAir = 28.97e-3;
                molRatio = molMass./molMassAir;
                %gridArea = tData.gridArea;
                tempData = tempData.*molRatio.*(1e-9).*airMass;
                tropMass(iSample,iTrac) = sum(tempData(isTrop));
                atmMass(iSample,iTrac) = sum(tempData(:));
            end
            clear tempData PEdge PCenter PTrop PSurf;
        end
        stratMass = atmMass - tropMass;
        end

    end
    
end
